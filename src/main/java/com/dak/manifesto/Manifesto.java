package com.dak.manifesto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/manifesto")
public class Manifesto extends HttpServlet {
   private static final long serialVersionUID = 9005224244559377508L;
   
   private static Properties props = null;
   private static final Gson gson = new Gson();

   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      response.setContentType("application/json");
      response.getWriter().print(getJsonString(getApplicationProperties(request))); 
   }
   
   private synchronized String getJsonString(Object o){
      return gson.toJson(o);
   }
   
   protected Properties getApplicationProperties(HttpServletRequest request){
      synchronized(props){
         if(props == null) {
            try{
               props = new Properties();
               String name = "/META-INF/MANIFEST.MF";
               props.load(request.getSession().getServletContext().getResourceAsStream(name));
            } catch (Exception e) {
               e.printStackTrace();
               // do nothing; don't want to break parent app
            }
         }
      }

      return props;
   }
}
