# Manifesto #

[![Build Status](https://travis-ci.org/davidkey/manifesto.svg?branch=master)](https://travis-ci.org/davidkey/manifesto)

### What is this repository for? ###

* Manifesto automatically adds a json endpoint to your web application with all values from your manifest file. Best used in combination with a build-populated manifest (such as maven-war-plugin -- manifestEntries).
* Version 1.0.1


### How do I get set up? ###

* `git clone https://davidkey@bitbucket.org/davidkey/manifesto.git`
* `mvn package`
* Upload to artifactory (local / remote) (ex: `mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=manifesto.jar`)
* Add to your web application's pom.xml:

```xml

<dependency>
   <groupId>com.dak</groupId>
   <artifactId>manifesto</artifactId>
   <version>1.0.1-RELEASE</version>
</dependency>`

```
* If you're using a  pre-3.0 servlet version, you will need to add a new servlet mapping to your web.xml:

```xml

<servlet>
   <servlet-name>manifestoServlet</servlet-name>
   <servlet-class>com.dak.manifesto.Manifesto</servlet-class>
   <load-on-startup>1</load-on-startup>
</servlet>
	
<servlet-mapping>
   <servlet-name>manifestoServlet</servlet-name>
   <url-pattern>/manifesto</url-pattern>
</servlet-mapping>

```
* Navigate to $WEB_ROOT/manifesto (overridable in servlet mapping) 


### Who do I talk to? ###

* dak (davidkey@gmail.com)
